"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/es.symbol");

require("core-js/modules/es.symbol.description");

require("core-js/modules/es.symbol.iterator");

require("core-js/modules/es.array.filter");

require("core-js/modules/es.array.for-each");

require("core-js/modules/es.array.from");

require("core-js/modules/es.array.iterator");

require("core-js/modules/es.array.reduce");

require("core-js/modules/es.array.reverse");

require("core-js/modules/es.array.slice");

require("core-js/modules/es.array.sort");

require("core-js/modules/es.function.name");

require("core-js/modules/es.object.entries");

require("core-js/modules/es.object.get-own-property-descriptor");

require("core-js/modules/es.object.get-own-property-descriptors");

require("core-js/modules/es.object.keys");

require("core-js/modules/es.object.to-string");

require("core-js/modules/es.regexp.constructor");

require("core-js/modules/es.regexp.exec");

require("core-js/modules/es.regexp.to-string");

require("core-js/modules/es.string.iterator");

require("core-js/modules/web.dom-collections.for-each");

require("core-js/modules/web.dom-collections.iterator");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var Mock = /*#__PURE__*/function () {
  function Mock() {
    (0, _classCallCheck2.default)(this, Mock);
    this.resReqs = [];
  }

  (0, _createClass2.default)(Mock, [{
    key: "query",
    value: function query(req) {
      var sortedResReqs = (0, _toConsumableArray2.default)(this.resReqs).sort(function (a, b) {
        return (a.priority || 0) - (b.priority || 0);
      }).reverse();

      var _iterator = _createForOfIteratorHelper(sortedResReqs),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var resReq = _step.value;

          var _this$getRes = this.getRes(resReq.reqTemplate, req, resReq.res),
              _this$getRes2 = (0, _slicedToArray2.default)(_this$getRes, 2),
              found = _this$getRes2[0],
              res = _this$getRes2[1];

          if (found) return res;
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    }
  }, {
    key: "getRes",
    value: function getRes(reqTemplate, req, res, maxDepth) {
      var _this = this;

      var depth = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
      if (typeof reqTemplate === 'undefined') return [false, undefined];
      if (req == reqTemplate) return [true, res];

      if (req.toString && reqTemplate instanceof RegExp && reqTemplate.test(req.toString())) {
        return [true, res];
      }

      if ((0, _typeof2.default)(reqTemplate) !== (0, _typeof2.default)(req)) return [false, undefined];

      if (Array.isArray(reqTemplate) && Array.isArray(req)) {
        return [false, undefined];
      }

      if ((0, _typeof2.default)(reqTemplate) === 'object') {
        var found = Object.entries(reqTemplate).reduce(function (found, _ref) {
          var _ref2 = (0, _slicedToArray2.default)(_ref, 2),
              key = _ref2[0],
              value = _ref2[1];

          if (found === false) return found;
          if (typeof value === 'undefined') return true;
          if (!(key in req)) return false;

          var _ref3 = typeof maxDepth === 'number' && depth >= maxDepth ? [true] : _this.getRes(reqTemplate[key], req[key], res, maxDepth, ++depth);

          var _ref4 = (0, _slicedToArray2.default)(_ref3, 1);

          found = _ref4[0];
          return found;
        }, undefined);
        if (found) return [true, res];
      }

      return [false, undefined];
    }
  }, {
    key: "register",
    value: function register(reqTemplate, res, options) {
      var _ref5 = _objectSpread({}, options),
          priority = _ref5.priority;

      this.resReqs.push({
        reqTemplate: reqTemplate,
        res: res,
        priority: priority
      });
    }
  }, {
    key: "reset",
    value: function reset() {
      this.resReqs = [];
    }
  }]);
  return Mock;
}();

exports.default = Mock;
//# sourceMappingURL=mock.js.map