/**
 * Copyright 2020 Nueve Solutions LLC
 * Copyright 2020 Q2 Holdings Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { IDict } from 'q2-tecton-sdk/dist/esm/types/utility';
import { IPictureOptions } from 'q2-tecton-sdk/dist/esm/sources/takePicture';
import { IPlatformModalOptions } from 'q2-tecton-sdk/dist/esm/actions/showModal';
import { IPlatformRequestBody } from 'q2-tecton-sdk/dist/esm/sources/requestPlatformData';
import { IExtensionResponse, IExtensionRequestBody } from 'q2-tecton-sdk/dist/esm/sources/requestExtensionData';
import { IPlatformAlertOptions, LogLevelStrings, OutletInfo, TectonResponse } from 'q2-tecton-sdk/dist/esm/types';
import { IPlatformCoreAPI, IActions, IConnectOptions, ISources } from 'q2-tecton-sdk';
interface IOverpanelOptions {
    fullWidth?: boolean;
}
export declare class Sources implements ISources {
    private _mocks;
    cacheCleared(_route: string, _callback: () => void): () => void;
    canUser(..._capabilities: string[]): Promise<boolean>;
    getCurrentLanguageCode(): string;
    getFeatureConfig(): Promise<IDict<any>>;
    getPlatformInfo(..._requestedPlatformInfo: string[]): Promise<IDict<any>>;
    onFetchPlatformData(route?: string | RegExp, priority?: number): {
        reply: (status: number, response?: any) => void;
    };
    fetchPlatformData(route: string): Promise<TectonResponse>;
    isNavigable(_featureName: string, _moduleName?: string): Promise<boolean>;
    languageChanged(_callback: (language?: string) => void): void;
    loc(_key: string, _substitutions?: string[] | IDict<any>, _option?: IDict<any>): string;
    paramsChanged(_callback: (params?: IDict<string>) => void): () => void;
    promptForMFA(_promptOptions?: {
        message?: string;
    }): Promise<void>;
    platformScrollChanged(_callback: () => void): () => void;
    refetchRequired(_dataType: string, _callback: () => void): () => void;
    onRequestExtensionData<R>(route?: string | RegExp, body?: IDict<any>, priority?: number): {
        reply: (status: number, data?: R | undefined) => void;
    };
    requestExtensionData?<R>(requestOptions?: IExtensionRequestBody): Promise<IExtensionResponse<R>>;
    requestOutletInfo(): Promise<OutletInfo>;
    onRequestPlatformData(data?: PlatformRequestBodyTemplate, priority?: number): {
        reply: (status: number, response?: any) => void;
    };
    requestPlatformData(requestOptions?: IPlatformRequestBody): Promise<TectonResponse>;
    t(_key: string, _substitutions?: string[] | IDict<any>, _option?: IDict<any>): string;
    takePicture(_pictureOptions?: IPictureOptions): Promise<string>;
}
export declare class Actions implements IActions {
    sendOverpanel(_featureModuleName: string, _params?: IDict<string>): Promise<void>;
    showOverpanel<T>(_featureModuleName: string, _params?: IDict<string>, _options?: IOverpanelOptions): Promise<T>;
    closeOverpanel(_resolutionData?: IDict<any>): void;
    showModal(_modalOptions: IPlatformModalOptions): Promise<string>;
    showAlert(_alertOptions: IPlatformAlertOptions): void;
    navigateTo(_featureName: string): void;
    navigateTo(_featureName: string, _moduleName: string): void;
    navigateTo(_featureName: string, _urlParams: any[]): void;
    navigateTo(_featureName: string, _queryParams: IDict<any>): void;
    navigateTo(_featureName: string, _urlParams: any[], _queryParams: IDict<any>): void;
    navigateTo(_featureName: string, _moduleName: string, _urlParams: any[]): void;
    navigateTo(_featureName: string, _moduleName: string, _queryParams: IDict<any>): void;
    navigateTo(_featureName: string, _moduleName: string, _urlParams: any[], _queryParams: IDict<any>): void;
    scrollToTop(): void;
    setTitle(_title: string): void;
    setParams(_params: IDict<string>): void;
    clearParams(): void;
    notifyRefetchAccounts(): void;
    printWindow(): void;
    keepAlive(): void;
    showLoadingModal(_title?: string): void;
    clearLoadingModal(): void;
    setFetching(_fetching?: boolean): void;
    logToServer(_message: string, _level?: LogLevelStrings): void;
    openURL(_url: string, _options: {
        showInOverpanel: boolean;
        nativeBrowserIfInApp: boolean;
    }): void;
    resizeIframe(): void;
    clearCache(..._routePrefixes: string[]): Promise<void>;
}
export interface Q2TectonMock extends IPlatformCoreAPI {
    sources?: Sources;
    actions?: Actions;
}
export declare function connect(_options?: IConnectOptions): Promise<Q2TectonMock>;
export declare type PlatformRequestBodyTemplate = {
    route?: string | RegExp;
} | {
    dataId?: string | RegExp;
    dataType: string | RegExp;
    params?: IDict<string | RegExp>;
};
export {};
