"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.connect = connect;
exports.Actions = exports.Sources = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _rxjs = require("rxjs");

var _mocks = require("./mocks");

/**
 * Copyright 2020 Nueve Solutions LLC
 * Copyright 2020 Q2 Holdings Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var Sources = /*#__PURE__*/function () {
  function Sources() {
    (0, _classCallCheck2.default)(this, Sources);
    this._mocks = (0, _mocks.createMocks)();
  }

  (0, _createClass2.default)(Sources, [{
    key: "cacheCleared",
    value: function cacheCleared(_route, _callback) {
      return function () {};
    }
  }, {
    key: "canUser",
    value: function canUser() {
      return true;
    }
  }, {
    key: "getCurrentLanguageCode",
    value: function getCurrentLanguageCode() {
      return '';
    }
  }, {
    key: "getFeatureConfig",
    value: function getFeatureConfig() {
      return {};
    }
  }, {
    key: "getPlatformInfo",
    value: function getPlatformInfo() {
      return {};
    }
  }, {
    key: "onFetchPlatformData",
    value: function onFetchPlatformData(route, priority) {
      var _this = this;

      return {
        reply: function reply(status, response) {
          _this._mocks.fetchPlatformData.register(route || /.*/, {
            response: response,
            status: status,
            statusText: status === 200 ? 'success' : 'failed'
          }, {
            priority: priority
          });
        }
      };
    }
  }, {
    key: "fetchPlatformData",
    value: function fetchPlatformData(route) {
      return this._mocks.fetchPlatformData.query(route);
    }
  }, {
    key: "isNavigable",
    value: function isNavigable(_featureName, _moduleName) {
      return true;
    }
  }, {
    key: "languageChanged",
    value: function languageChanged(_callback) {}
  }, {
    key: "loc",
    value: function loc(_key, _substitutions, _option) {
      return '';
    }
  }, {
    key: "paramsChanged",
    value: function paramsChanged(_callback) {
      return function () {};
    }
  }, {
    key: "promptForMFA",
    value: function promptForMFA(_promptOptions) {
      return undefined;
    }
  }, {
    key: "platformScrollChanged",
    value: function platformScrollChanged(_callback) {
      return function () {};
    }
  }, {
    key: "refetchRequired",
    value: function refetchRequired(_dataType, _callback) {
      return function () {};
    }
  }, {
    key: "onRequestExtensionData",
    value: function onRequestExtensionData(route, body, priority) {
      var _this2 = this;

      return {
        reply: function reply(status, data) {
          _this2._mocks.requestExtensionData.register({
            route: route || /.*/,
            body: body
          }, {
            data: data,
            status: status,
            statusText: status === 200 ? 'success' : 'failed'
          }, {
            priority: priority
          });
        }
      };
    }
  }, {
    key: "requestExtensionData",
    value: function requestExtensionData(requestOptions) {
      return this._mocks.requestExtensionData.query(requestOptions);
    }
  }, {
    key: "requestOutletInfo",
    value: function requestOutletInfo() {
      return {
        bottom: 0,
        height: 0,
        left: 0,
        right: 0,
        top: 0,
        width: 0,
        x: 0,
        y: 0
      };
    }
  }, {
    key: "onRequestPlatformData",
    value: function onRequestPlatformData() {
      var _this3 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
        route: /.*/g
      };
      var priority = arguments.length > 1 ? arguments[1] : undefined;
      return {
        reply: function reply(status, response) {
          _this3._mocks.fetchPlatformData.register(data, {
            response: response,
            status: status,
            statusText: status === 200 ? 'success' : 'failed'
          }, {
            priority: priority
          });
        }
      };
    }
  }, {
    key: "requestPlatformData",
    value: function requestPlatformData(requestOptions) {
      return this._mocks.requestPlatformData.query(requestOptions);
    }
  }, {
    key: "t",
    value: function t(_key, _substitutions, _option) {
      return '';
    }
  }, {
    key: "takePicture",
    value: function takePicture(_pictureOptions) {
      return '';
    }
  }]);
  return Sources;
}();

exports.Sources = Sources;

var Actions = /*#__PURE__*/function () {
  function Actions() {
    (0, _classCallCheck2.default)(this, Actions);
  }

  (0, _createClass2.default)(Actions, [{
    key: "sendOverpanel",
    value: function sendOverpanel(_featureModuleName, _params) {
      return undefined;
    }
  }, {
    key: "showOverpanel",
    value: function showOverpanel(_featureModuleName, _params, _options) {
      return {};
    }
  }, {
    key: "closeOverpanel",
    value: function closeOverpanel(_resolutionData) {}
  }, {
    key: "showModal",
    value: function showModal(_modalOptions) {
      return '';
    }
  }, {
    key: "showAlert",
    value: function showAlert(_alertOptions) {}
  }, {
    key: "navigateTo",
    value: function navigateTo(_featureName, _moduleName, _urlParams, _queryParams) {}
  }, {
    key: "scrollToTop",
    value: function scrollToTop() {}
  }, {
    key: "setTitle",
    value: function setTitle(_title) {}
  }, {
    key: "setParams",
    value: function setParams(_params) {}
  }, {
    key: "clearParams",
    value: function clearParams() {}
  }, {
    key: "notifyRefetchAccounts",
    value: function notifyRefetchAccounts() {}
  }, {
    key: "printWindow",
    value: function printWindow() {}
  }, {
    key: "keepAlive",
    value: function keepAlive() {}
  }, {
    key: "showLoadingModal",
    value: function showLoadingModal(_title) {}
  }, {
    key: "clearLoadingModal",
    value: function clearLoadingModal() {}
  }, {
    key: "setFetching",
    value: function setFetching(_fetching) {}
  }, {
    key: "logToServer",
    value: function logToServer(_message, _level) {}
  }, {
    key: "openURL",
    value: function openURL(_url, _options) {}
  }, {
    key: "resizeIframe",
    value: function resizeIframe() {}
  }, {
    key: "clearCache",
    value: function clearCache() {
      return undefined;
    }
  }]);
  return Actions;
}();

exports.Actions = Actions;

function connect(_options) {
  var actions = new Actions();
  var sources = new Sources();
  return {
    actions: actions,
    outletContext: '',
    params$: new _rxjs.BehaviorSubject({}),
    sources: sources,
    teardown: function teardown() {
      sources._mocks.requestExtensionData.reset();

      sources._mocks.requestPlatformData.reset();

      sources._mocks.fetchPlatformData.reset();
    }
  };
}
//# sourceMappingURL=index.js.map