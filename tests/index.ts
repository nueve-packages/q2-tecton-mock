/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BehaviorSubject } from 'rxjs';
import { IDict } from 'q2-tecton-sdk/dist/esm/types/utility';
import { connect, Actions, Sources, Q2TectonMock } from '~/index';

describe('await connect()', () => {
  let tecton: Q2TectonMock;
  beforeEach(async () => {
    tecton = await connect();
  });

  afterEach(() => tecton.teardown());

  it('should create tecton api', async () => {
    expect(JSON.stringify(tecton)).toEqual(
      JSON.stringify({
        actions: new Actions(),
        outletContext: '',
        params$: new BehaviorSubject<IDict<string>>({}),
        sources: new Sources(),
        teardown() {}
      })
    );
  });
});

describe('await connect().onRequestExtensionData()', () => {
  let tecton: Q2TectonMock;
  beforeEach(async () => {
    tecton = await connect();
  });

  afterEach(() => tecton.teardown());

  it('should register extension data', async () => {
    tecton.sources
      ?.onRequestExtensionData('hello')
      .reply(200, { howdy: 'texas' });
    const result = await tecton.sources?.requestExtensionData!({
      route: 'hello'
    });
    expect(result).toMatchObject({
      status: 200,
      statusText: 'success',
      data: {
        howdy: 'texas'
      }
    });
  });

  it("should return 'undefined' when not found", async () => {
    tecton.sources
      ?.onRequestExtensionData('hello', { hello: 'world' })
      .reply(200, { howdy: 'texas' });
    const result = await tecton.sources?.requestExtensionData!({
      route: 'howdy'
    });
    expect(result).toBe(undefined);
  });

  it('should support global handler', async () => {
    tecton.sources?.onRequestExtensionData().reply(404);
    tecton.sources
      ?.onRequestExtensionData('hello', { hello: 'world' })
      .reply(200, { howdy: 'texas' });
    const result = await tecton.sources?.requestExtensionData!({
      route: 'howdy'
    });
    expect(result).toMatchObject({
      status: 404,
      statusText: 'failed'
    });
  });

  it('should support order', async () => {
    tecton.sources?.onRequestExtensionData().reply(404);
    tecton.sources
      ?.onRequestExtensionData('hello', { hello: 'world' })
      .reply(200, { howdy: 'texas' });
    const result = await tecton.sources?.requestExtensionData!({
      route: 'hello',
      body: { hello: 'world' }
    });
    expect(result).toMatchObject({
      status: 200,
      statusText: 'success',
      data: {
        howdy: 'texas'
      }
    });
  });

  it('should support priority', async () => {
    tecton.sources
      ?.onRequestExtensionData('hello')
      .reply(200, { howdy: 'texas' });
    tecton.sources?.onRequestExtensionData(undefined, undefined, -1).reply(404);
    const result = await tecton.sources?.requestExtensionData!({
      route: 'hello',
      body: { hello: 'world' }
    });
    expect(result).toMatchObject({
      status: 200,
      statusText: 'success',
      data: {
        howdy: 'texas'
      }
    });
  });

  it('should support regex matching', async () => {
    tecton.sources
      ?.onRequestExtensionData(/^h\w{4}$/)
      .reply(200, { howdy: 'texas' });
    tecton.sources?.onRequestExtensionData(undefined, undefined, -1).reply(404);
    let result = await tecton.sources?.requestExtensionData!({
      route: 'hello',
      body: { hello: 'world' }
    });
    expect(result).toMatchObject({
      status: 200,
      statusText: 'success',
      data: {
        howdy: 'texas'
      }
    });
    result = await tecton.sources?.requestExtensionData!({
      route: 'howdy'
    });
    expect(result).toMatchObject({
      status: 200,
      statusText: 'success',
      data: {
        howdy: 'texas'
      }
    });
    result = await tecton.sources?.requestExtensionData!({
      route: 'bonjour'
    });
    expect(result).toMatchObject({
      status: 404,
      statusText: 'failed'
    });
  });

  it('should support properties', async () => {
    tecton.sources
      ?.onRequestExtensionData(/^h\w{4}$/, { hello: /^world$/ })
      .reply(200, { howdy: 'texas' });
    tecton.sources?.onRequestExtensionData(undefined, undefined, -1).reply(404);
    let result = await tecton.sources?.requestExtensionData!({
      route: 'hello',
      body: { hello: 'world' }
    });
    expect(result).toMatchObject({
      status: 200,
      statusText: 'success',
      data: { howdy: 'texas' }
    });
    result = await tecton.sources?.requestExtensionData!({
      route: 'hello'
    });
    expect(result).toMatchObject({
      status: 404,
      statusText: 'failed'
    });
    result = await tecton.sources?.requestExtensionData!({
      route: 'howdy',
      body: { hello: 'texas' }
    });
    expect(result).toMatchObject({
      status: 404,
      statusText: 'failed'
    });
  });
});
