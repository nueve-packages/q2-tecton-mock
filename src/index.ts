/**
 * Copyright 2020 Nueve Solutions LLC
 * Copyright 2020 Q2 Holdings Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BehaviorSubject } from 'rxjs';
import { IDict } from 'q2-tecton-sdk/dist/esm/types/utility';
import { IPictureOptions } from 'q2-tecton-sdk/dist/esm/sources/takePicture';
import { IPlatformModalOptions } from 'q2-tecton-sdk/dist/esm/actions/showModal';
import { IPlatformRequestBody } from 'q2-tecton-sdk/dist/esm/sources/requestPlatformData';
import {
  IExtensionResponse,
  IExtensionRequestBody
} from 'q2-tecton-sdk/dist/esm/sources/requestExtensionData';
import {
  IPlatformAlertOptions,
  LogLevelStrings,
  OutletInfo,
  TectonResponse
} from 'q2-tecton-sdk/dist/esm/types';
import {
  IPlatformCoreAPI,
  IActions,
  IConnectOptions,
  ISources
} from 'q2-tecton-sdk';
import { createMocks } from './mocks';
import { ReqTemplate } from './mock';

interface IOverpanelOptions {
  fullWidth?: boolean;
}

export class Sources implements ISources {
  private _mocks = createMocks();

  cacheCleared(_route: string, _callback: () => void): () => void {
    return () => {};
  }

  canUser(..._capabilities: string[]): Promise<boolean> {
    return true as any;
  }

  getCurrentLanguageCode(): string {
    return '';
  }

  getFeatureConfig(): Promise<IDict<any>> {
    return {} as any;
  }

  getPlatformInfo(..._requestedPlatformInfo: string[]): Promise<IDict<any>> {
    return {} as any;
  }

  onFetchPlatformData(route?: string | RegExp, priority?: number) {
    return {
      reply: (status: number, response?: any) => {
        this._mocks.fetchPlatformData.register(
          route || /.*/,
          {
            response,
            status,
            statusText: status === 200 ? 'success' : 'failed'
          },
          { priority }
        );
      }
    };
  }

  fetchPlatformData(route: string): Promise<TectonResponse> {
    return this._mocks.fetchPlatformData.query(route) as any;
  }

  isNavigable(_featureName: string, _moduleName?: string): Promise<boolean> {
    return true as any;
  }

  languageChanged(_callback: (language?: string) => void): void {}

  loc(
    _key: string,
    _substitutions?: string[] | IDict<any>,
    _option?: IDict<any>
  ): string {
    return '';
  }

  paramsChanged(_callback: (params?: IDict<string>) => void): () => void {
    return () => {};
  }

  promptForMFA(_promptOptions?: { message?: string }): Promise<void> {
    return undefined as any;
  }

  platformScrollChanged(_callback: () => void): () => void {
    return () => {};
  }

  refetchRequired(_dataType: string, _callback: () => void): () => void {
    return () => {};
  }

  onRequestExtensionData<R>(
    route?: string | RegExp,
    body?: IDict<any>,
    priority?: number
  ) {
    return {
      reply: (status: number, data?: R) => {
        this._mocks.requestExtensionData.register(
          {
            route: route || /.*/,
            body
          },
          {
            data,
            status,
            statusText: status === 200 ? 'success' : 'failed'
          },
          { priority }
        );
      }
    };
  }

  requestExtensionData?<R>(
    requestOptions?: IExtensionRequestBody
  ): Promise<IExtensionResponse<R>> {
    return this._mocks.requestExtensionData.query(requestOptions) as any;
  }

  requestOutletInfo(): Promise<OutletInfo> {
    return {
      bottom: 0,
      height: 0,
      left: 0,
      right: 0,
      top: 0,
      width: 0,
      x: 0,
      y: 0
    } as any;
  }

  onRequestPlatformData(
    data: PlatformRequestBodyTemplate = { route: /.*/g },
    priority?: number
  ) {
    return {
      reply: (status: number, response?: any) => {
        this._mocks.fetchPlatformData.register(
          data as ReqTemplate,
          {
            response,
            status,
            statusText: status === 200 ? 'success' : 'failed'
          },
          { priority }
        );
      }
    };
  }

  requestPlatformData(
    requestOptions?: IPlatformRequestBody
  ): Promise<TectonResponse> {
    return this._mocks.requestPlatformData.query(requestOptions) as any;
  }

  t(
    _key: string,
    _substitutions?: string[] | IDict<any>,
    _option?: IDict<any>
  ): string {
    return '';
  }

  takePicture(_pictureOptions?: IPictureOptions): Promise<string> {
    return '' as any;
  }
}

export class Actions implements IActions {
  sendOverpanel(
    _featureModuleName: string,
    _params?: IDict<string>
  ): Promise<void> {
    return undefined as any;
  }

  showOverpanel<T>(
    _featureModuleName: string,
    _params?: IDict<string>,
    _options?: IOverpanelOptions
  ): Promise<T> {
    return {} as any;
  }

  closeOverpanel(_resolutionData?: IDict<any>): void {}

  showModal(_modalOptions: IPlatformModalOptions): Promise<string> {
    return '' as any;
  }

  showAlert(_alertOptions: IPlatformAlertOptions): void {}

  navigateTo(_featureName: string): void;
  navigateTo(_featureName: string, _moduleName: string): void;
  navigateTo(_featureName: string, _urlParams: any[]): void;
  navigateTo(_featureName: string, _queryParams: IDict<any>): void;
  navigateTo(
    _featureName: string,
    _urlParams: any[],
    _queryParams: IDict<any>
  ): void;
  navigateTo(
    _featureName: string,
    _moduleName: string,
    _urlParams: any[]
  ): void;
  navigateTo(
    _featureName: string,
    _moduleName: string,
    _queryParams: IDict<any>
  ): void;
  navigateTo(
    _featureName: string,
    _moduleName: string,
    _urlParams: any[],
    _queryParams: IDict<any>
  ): void;
  navigateTo(
    _featureName: string,
    _moduleName?: string | any[] | IDict<any>,
    _urlParams?: any[] | IDict<any>,
    _queryParams?: IDict<any>
  ): void {}

  scrollToTop(): void {}

  setTitle(_title: string): void {}

  setParams(_params: IDict<string>): void {}

  clearParams(): void {}

  notifyRefetchAccounts(): void {}

  printWindow(): void {}

  keepAlive(): void {}

  showLoadingModal(_title?: string): void {}

  clearLoadingModal(): void {}

  setFetching(_fetching?: boolean): void {}

  logToServer(_message: string, _level?: LogLevelStrings): void {}

  openURL(
    _url: string,
    _options: {
      showInOverpanel: boolean;
      nativeBrowserIfInApp: boolean;
    }
  ): void {}

  resizeIframe(): void {}

  clearCache(..._routePrefixes: string[]): Promise<void> {
    return undefined as any;
  }
}

export interface Q2TectonMock extends IPlatformCoreAPI {
  sources?: Sources;
  actions?: Actions;
}

export function connect(_options?: IConnectOptions): Promise<Q2TectonMock> {
  const actions = new Actions();
  const sources = new Sources();
  return {
    actions,
    outletContext: '',
    params$: new BehaviorSubject<IDict<string>>({}),
    sources,
    teardown() {
      (sources as any)._mocks.requestExtensionData.reset();
      (sources as any)._mocks.requestPlatformData.reset();
      (sources as any)._mocks.fetchPlatformData.reset();
    }
  } as any;
}

export type PlatformRequestBodyTemplate =
  | {
      route?: string | RegExp;
    }
  | {
      dataId?: string | RegExp;
      dataType: string | RegExp;
      params?: IDict<string | RegExp>;
    };
