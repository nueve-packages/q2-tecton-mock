/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default class Mock<Req, Res> {
  resReqs: ResReq<Res>[] = [];

  query(req: Req): Res | undefined {
    const sortedResReqs = [...this.resReqs]
      .sort((a, b) => (a.priority || 0) - (b.priority || 0))
      .reverse();
    for (const resReq of sortedResReqs) {
      const [found, res] = this.getRes(resReq.reqTemplate, req, resReq.res);
      if (found) return res;
    }
  }

  getRes(
    reqTemplate: ReqTemplate,
    req: Req,
    res?: Res,
    maxDepth?: number,
    depth = 0
  ): [boolean, Res | undefined] {
    if (typeof reqTemplate === 'undefined') return [false, undefined];
    if ((req as any) == reqTemplate) return [true, res];
    if (
      (req as any).toString &&
      reqTemplate instanceof RegExp &&
      reqTemplate.test((req as any).toString())
    ) {
      return [true, res];
    }
    if (typeof reqTemplate !== typeof req) return [false, undefined];
    if (Array.isArray(reqTemplate) && Array.isArray(req)) {
      return [false, undefined];
    }
    if (typeof reqTemplate === 'object') {
      const found = Object.entries(reqTemplate).reduce(
        (found: boolean | undefined, [key, value]: [string, any]) => {
          if (found === false) return found;
          if (typeof value === 'undefined') return true;
          if (!(key in req)) return false;
          [found] =
            typeof maxDepth === 'number' && depth >= maxDepth
              ? [true]
              : this.getRes(
                  (reqTemplate as any)[key],
                  (req as any)[key],
                  res,
                  maxDepth,
                  ++depth
                );
          return found;
        },
        undefined
      );
      if (found) return [true, res];
    }
    return [false, undefined];
  }

  register(
    reqTemplate: ReqTemplate,
    res: Res,
    options: Partial<RegisterOptions>
  ) {
    const { priority } = { ...options } as RegisterOptions;
    this.resReqs.push({ reqTemplate, res, priority });
  }

  reset() {
    this.resReqs = [];
  }
}

export type ReqTemplate =
  | undefined
  | string
  | boolean
  | number
  | RegExp
  | IReqTemplate;

export interface IReqTemplate {
  [key: string]: ReqTemplate;
}

export interface ResReq<Res> {
  priority?: number;
  reqTemplate: ReqTemplate;
  res: Res | undefined;
}

export interface RegisterOptions {
  priority?: number;
}
