# q2-tecton-mock

[![GitHub stars](https://img.shields.io/github/stars/nuevesolutions/q2-tecton-mock.svg?style=social&label=Stars)](https://github.com/nuevesolutions/q2-tecton-mock)

> mock q2 tecton api

Please ★ this repo if you found it useful ★ ★ ★

## Installation

```sh
npm install --save q2-tecton-mock
```

## Support

Submit an [issue](https://github.com/nuevesolutions/q2-tecton-mock/issues/new)

## Contributing

Review the [guidelines for contributing](https://github.com/nuevesolutions/q2-tecton-mock/blob/master/CONTRIBUTING.md)

## License

[MIT License](https://github.com/nuevesolutions/q2-tecton-mock/blob/master/LICENSE)

[Nueve Solutions LLC](https://nuevesolutions.com) © 2020

## Changelog

Review the [changelog](https://github.com/nuevesolutions/q2-tecton-mock/blob/master/CHANGELOG.md)

## Contributors

- [Jam Risser](https://codejam.ninja) - Author
